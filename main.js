var input = require("readline-sync");
const Urheilija = require("./Urheilija.js");

const urheilija1 = new Urheilija("Matti", "Mattinen", "Masa", "1980",
 "www.linkki.fi", 79, "juoksu", ["Kultamitali 2010", "1. Sija 2018"])

console.log(urheilija1);

console.log("Vaihdetaan linkkia");
urheilija1.kuva = "www.uusilinkki.fi";

console.log("Urheilijan 1 uusi kuvalinkki: " + urheilija1.kuva);
console.log("Urheilijan etunimi on " + urheilija1.etuNimi);
console.log("Urheilijan sukunimi oli " + urheilija1.sukuNimi);
urheilija1.sukuNimi = "Makinen";
console.log("Nyt urheilijan sukunimi onkin " + urheilija1.sukuNimi);