'use strict';
class Henkilo {
    //Constructor
    constructor(etuNimi, sukuNimi, kutsumaNimi, syntymaVuosi) {
        this._etuNimi = etuNimi;
        this._sukuNimi = sukuNimi;
        this._kutsumaNimi = kutsumaNimi;
        this._syntymaVuosi = syntymaVuosi;
    }

    //Getters
    get etuNimi() {
        return this._etuNimi;
    }

    get sukuNimi() {
        return this._sukuNimi;
    }

    get kutsumaNimi() {
        return this._kutsumaNimi;
    }

    get syntymaVuosi() {
        return this._syntymaVuosi;
    }

    //Setters
    set etuNimi(n) {
        this._etuNimi = n;
    }

    set sukuNimi(n) {
        this._sukuNimi = n;
    }

    set kutsumaNimi(n) {
        this._kutsumaNimi = n;
    }

    set syntymaVuosi(n) {
        this._syntymaVuosi = n;
    }
};
module.exports = Henkilo;