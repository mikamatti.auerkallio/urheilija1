'use strict';
const Henkilo = require("./Henkilo.js");
class Urheilija extends Henkilo {
    //Constructor
    constructor(etuNimi, sukuNimi, kutsumaNimi, syntymaVuosi, kuvaLinkki, paino, laji, saavutukset) {
        super(etuNimi, sukuNimi, kutsumaNimi, syntymaVuosi);
        this._kuva = kuvaLinkki;
        this._omaPaino = paino;
        this._laji = laji;
        this._saavutukset = saavutukset;
    }

    //Getters
    get kuva() {
        return this._kuva;
    }

    get omaPaino() {
        return this._omaPaino;
    }

    get laji() {
        return this._laji;
    }

    get saavutukset() {
        return this._saavutukset;
    }

    //Setters
    set kuva(n) {
        this._kuva = n;
    }

    set omaPaino(n) {
        this._omaPaino = n;
    }

    set laji(n) {
        this._laji = n;
    }

    set saavutukset(n) {
        this._saavutukset = n;
    }
};
module.exports = Urheilija;